<?php

use App\Http\Controllers\AlbumController;
use App\Http\Controllers\GalleryController;
use App\Http\Controllers\GalleryImageController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => ['auth']], function() {
    Route::resource('albums', AlbumController::class);
    Route::resource('galleries', GalleryController::class);
    // Route::resource('galleries/{gallery}/galleryimages', GalleryImageController::class);
    Route::get('galleries/{gallery}/galleryimages', [GalleryImageController::class, 'index'])->name('galleryimages.index');
    Route::post('galleries/{gallery}/galleryimages', [GalleryImageController::class, 'store'])->name('galleryimages.store');
    Route::get('galleries/{gallery}/galleryimages/create', [GalleryImageController::class, 'create'])->name('galleryimages.create');
    Route::put('galleries/{gallery}/galleryimages/update', [GalleryImageController::class, 'update'])->name('galleryimages.update');
    Route::put('galleries/{gallery}/galleryimages/updatesortorder', [GalleryImageController::class, 'updateSortOrder'])->name('galleryimages.updatesortorder');
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
