<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'published_by', 'album_id'];

    public function galleryImages()
    {
        return $this->hasMany(GalleryImage::class, 'gallery_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'published_by', 'id');
    }

    public function album()
    {
        return $this->belongsTo(Album::class, 'album_id', 'id');
    }
}
