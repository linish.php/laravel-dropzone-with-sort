<?php

namespace App\Http\Controllers;

use App\Models\Gallery;
use App\Models\GalleryImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class GalleryImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Gallery $gallery)
    {
        $galleryImages = $gallery->galleryImages()->orderBy('sort_order', 'asc')->get(); // Getting gallery images based on gallery

        return view('galleries.gallery_images.index', compact('gallery', 'galleryImages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Gallery $gallery)
    {
        return view('galleries.gallery_images.create', compact('gallery'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $gallery_id)
    {
        $images = $request->file('file');
        $completed_files = array();
        $sort_order = intval(GalleryImage::max('sort_order')) == 0 ? 1 : intval(GalleryImage::max('sort_order')) + 1;
        foreach($images as $image) {
            $file_name = uniqid().'.'.$image->getClientOriginalExtension();
            $path = public_path('uploads/gallery/'.$gallery_id);
    
            if (! File::exists($path)) {
                File::makeDirectory($path);
            }
    
            $image->move($path,$file_name);
                
            $imageUpload = new GalleryImage();
            $imageUpload->image = $file_name;
            $imageUpload->gallery_id = $gallery_id;
            $imageUpload->sort_order = $sort_order;
            $imageUpload->save();

            $completed_files[] = $file_name;
            $sort_order++;
        }

        return response()->json(['success' => $completed_files]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GalleryImage  $galleryImage
     * @return \Illuminate\Http\Response
     */
    public function show(GalleryImage $galleryImage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GalleryImage  $galleryImage
     * @return \Illuminate\Http\Response
     */
    public function edit(GalleryImage $galleryImage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GalleryImage  $galleryImage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Gallery $gallery, GalleryImage $galleryImage)
    {
        $deletingImages = $request->post('deleted_images');

        if($deletingImages) {
            $imageIds = explode(',', $deletingImages);
            foreach($imageIds as $imageId) {
                $imageData = GalleryImage::find($imageId);
                $imageName = $imageData['image'];
                $filePath = public_path('uploads/gallery/'.$gallery->id.'/'.$imageName);
                if(File::exists($filePath)){
                    if(File::delete($filePath)) {
                        $imageData->delete($imageId);
                    }
                }
            }
        }

        $captions = $request->post('caption');
        if($captions) {
            foreach($captions as $id => $caption) {
                $galleryImageData = [
                    'caption' => $caption
                ];
                GalleryImage::find($id)->update($galleryImageData);
            }
        }
        return redirect()->route('galleryimages.index', $gallery)
                         ->with('success', 'Gallery updated successfully');
    }

    public function updateSortOrder(Gallery $gallery) {
        $sortedDatas = request()->post('sortedData');
        $sort_order = 1;

        foreach($sortedDatas as $sortedData) {
            $galleryImage = GalleryImage::find($sortedData);
            $galleryImage->update([
                'sort_order' => $sort_order
            ]);
            $sort_order++;
        }

        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GalleryImage  $galleryImage
     * @return \Illuminate\Http\Response
     */
    public function destroy(GalleryImage $galleryImage)
    {
        //
    }
}
