<?php

namespace App\Http\Controllers;

use App\Models\Album;
use App\Models\Gallery;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $galleries = Gallery::with('album')->get();

        $index = 0;

        return view('galleries.index', compact('galleries', 'index'));  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $albums = Album::all();
        return view('galleries.create', compact('albums'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $galleryData = $this->validate($request, [
            'name' => 'required',
            'album_id' => 'required',
        ]);

        $galleryData['published_by'] = $request->post('published_by');    

        Gallery::create($galleryData);
        return redirect()->route('galleries.index')
                         ->with('success', 'Gallery added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function show(Gallery $gallery)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function edit(Gallery $gallery)
    {
        $albums = Album::all();
        return view('galleries.edit', compact('gallery', 'albums'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Gallery $gallery)
    {
        $galleryData = $this->validate($request, [
            'name' => 'required',
            'album_id' => 'required',
        ]);

        $galleryData['published_by'] = $request->post('published_by');    

        $gallery->update($galleryData);
        return redirect()->route('galleries.index')
                         ->with('success', 'Gallery updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function destroy(Gallery $gallery)
    {
        if($gallery->galleryImages()->count() == 0) {
            $gallery->delete();
            return redirect()->route('galleries.index')
                         ->with('success', 'Gallery deleted successfully');
        }
        return redirect()->route('galleries.index')
                         ->with('error', 'Gallery is associated with an image!');
    }
}
