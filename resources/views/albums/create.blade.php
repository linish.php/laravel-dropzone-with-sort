@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="float-left">
            <h2>New Album</h2>
        </div>
        <div class="float-right">
            <a class="btn btn-primary" href="{{ route('albums.index') }}">Back</a>
        </div>
    </div>
</div>

@if(count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{ route('albums.store') }}" method="POST">
    @csrf
    <div class="row">
        <div class="col-xs-12 col-sm-12 cl-md-12">
            <div class="form-group">
                <strong>Album Name:</strong>
                <input type="text" name="name" id="name" placeholder="Album Name" class="form-control" value="{{ old('name') }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 cl-md-12">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </div>
</form>

@endsection