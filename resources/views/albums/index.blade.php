@extends('layouts.app')

@section('header-css')
    <link rel="stylesheet" href="{{ asset('css/vendor/datatable/datatables.min.css') }}">
@endsection

@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="float-left">
            <h2>Albums</h2>
        </div>
        <div class="float-right mb-4">
            <a class="btn btn-success" href="{{ route('albums.create') }}">Create New Album</a>
        </div>
    </div>
</div>

@if($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif

@if($message = Session::get('error'))
    <div class="alert alert-danger">
        <p>{{ $message }}</p>
    </div>
@endif

<table class="table table-striped table-bordered" id="datatable" style="width: 100%;">
    <thead>
        <tr>
            <th>No.</th>
            <th>Album</th>
            <th width="280px">Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse($albums as $key => $album)
            <tr>
                <td>{{ ++$index }}</td>
                <td>{{ $album->name }}</td>
                <td>
                    <a class="btn btn-primary" href="{{ route('albums.edit', $album->id) }}">Edit</a>
                    <form method="POST" action="{{ route('albums.destroy', $album->id) }}" style="display:inline">
                        @csrf
                        @method('DELETE')
                        <input type="submit" value="Delete" class="btn btn-danger show_confirm">
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td class="text-center" colspan="6">No Albums to list.</td>
            </tr>
        @endforelse
    </tbody>
</table>
@endsection

@section('footer-scripts')
    <script type="text/javascript" src="{{ asset('js/vendor/datatable/dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/vendor/datatable/dataTables.bootstrap4.min.js')}}"></script>
    <script type="text/javascript">
        jQuery('#datatable').DataTable();
    </script>
@endsection