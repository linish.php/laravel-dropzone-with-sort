@extends('layouts.app')

@section('header-css')
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/min/dropzone.min.css" integrity="sha512-bbUR1MeyQAnEuvdmss7V2LclMzO+R9BzRntEE57WIKInFVQjvX7l7QZSxjNDt8bg41Ww05oHSh0ycKFijqD7dA==" crossorigin="anonymous" /> --}}
@endsection

@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="float-left">
            <h2>New Gallery</h2>
        </div>
        <div class="float-right">
            <a class="btn btn-primary" href="{{ route('galleries.index') }}">Back</a>
        </div>
    </div>
</div>

@if(count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{ route('galleries.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-xs-12 col-sm-12 cl-md-12">
            <div class="form-group">
                <strong>Gallery Name:</strong>
                <input type="text" name="name" id="name" placeholder="Gallery Name" required class="form-control" value="{{ old('name') }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 cl-md-12">
            <div class="form-group">
                <strong>Album:</strong>
                <select class="form-control" name="album_id" id="year" required>
                    <option value="">Select Album</option>
                    @foreach($albums as $album)
                        <option value="{{$album->id}}">{{$album->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        {{-- <div class="col-xs-12 col-sm-12 cl-md-12">
            <div class="card mb-4">
                <div class="card-header">Select Images</div>
                <div class="card-body">
                    <div id="gallery-dropzone" class="dropzone"></div>
                </div>
            </div>
        </div> --}}
        <div class="col-xs-12 col-sm-12 cl-md-12">
            <input type="hidden" name="published_by" value="{{ auth()->user()->id }}">
            <button type="submit" id="upload-file-and-submit" class="btn btn-primary">Save</button>
        </div>
    </div>
</form>

@endsection

@section('footer-scripts')
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/min/dropzone.min.js" integrity="sha512-9WciDs0XP20sojTJ9E7mChDXy6pcO0qHpwbEJID1YVavz2H6QBz5eLoDD8lseZOb2yGT8xDNIV7HIe1ZbuiDWg==" crossorigin="anonymous"></script> --}}
@endsection

@section('extra-footer-scripts')
    <script>
        // Dropzone.autoDiscover = false;
        // var myDropzone = new Dropzone("#gallery-dropzone", { 
        //     url: '',
        //     addRemoveLinks: true,
        //     autoProcessQueue: false,
        //     maxFiles: 20,
        //     maxFilesize:5,//MB
        //     init: function () {
        //         this.on(
        //             "addedfile", function(file) {
        //                 caption = file.caption == undefined ? "" : file.caption;
        //                 file._captionLabel = Dropzone.createElement("<p>File Info:</p>")
        //                 file._captionBox = Dropzone.createElement("<input id='"+file.filename+"' type='text' name='caption' placeholder='Enter Caption' value="+caption+" >");
        //                 file.previewElement.appendChild(file._captionLabel);
        //                 file.previewElement.appendChild(file._captionBox);
        //             }
        //         ),
        //         this.on(
        //             "sending", function(file, xhr, formData){
        //                 formData.append('caption',file._captionBox.value);
        //             }
        //         )
        //     }
        // });     

        // $('#upload-file-and-submit').click(function() {
        //     myDropzone.processQueue();
        // });
    </script>
@endsection