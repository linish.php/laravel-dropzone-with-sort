@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="float-left">
            <h2>Edit Gallery</h2>
        </div>
        <div class="float-right">
            <a class="btn btn-primary" href="{{ route('galleries.index') }}">Back</a>
        </div>
    </div>
</div>

@if(count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{ route('galleries.update', $gallery->id) }}" method="POST">
    @csrf
    @method('PUT')
    <div class="row">
        <div class="col-xs-12 col-sm-12 cl-md-12">
            <div class="form-group">
                <strong>Gallery Name:</strong>
                <input type="text" name="name" id="name" placeholder="Gallery Name" class="form-control" value="{{ $gallery->name }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 cl-md-12">
            <div class="form-group">
                <strong>Album:</strong>
                <select class="form-control" name="album_id" id="year">
                    <option value="">Select Album</option>
                    @foreach($albums as $album)
                        <option @if(isset($gallery)) @if($album->id == $gallery->album_id) selected='selected' @endif @endif  value="{{$album->id}}">{{$album->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 cl-md-12">
            <input type="hidden" name="published_by" value="{{ auth()->user()->id }}">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </div>
</form>

@endsection