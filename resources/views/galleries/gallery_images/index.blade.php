@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-left">
                <h2>Gallery Images</h2>
            </div>
            <div class="float-right mb-4">
                <a class="btn btn-success" href="{{ route('galleryimages.create', $gallery) }}">Add Images</a>
            </div>
        </div>
    </div>

    @if($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    @if(count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="row">
        <div class="col-xs-12 col-sm-12 cl-md-12">                                
            <form action="{{ route('galleryimages.update', $gallery) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="card mb-4">
                    <div class="card-header">Existing Images</div>
                    <div class="card-body">
                            <div class="row" id="sortable">
                                @forelse ($galleryImages as $key => $galleryImage)
                                    <div id="{{ $galleryImage->id }}" class="col-md-3 mb-4 gallery-images relative image-{{ $galleryImage->id }}">
                                        <button type="button" class="close-image image-{{ $galleryImage->id }}" aria-label="Close" data-image-id ="{{ $galleryImage->id }}">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <img class="mb-4 block" style="border-radius:20px;" src="{{ asset('uploads/gallery/'.$gallery->id.'/'.$galleryImage->image) }}" alt="your avatar" width="100%" height="150px">
                                        <input type="text" class="form-control" name="caption[{{ $galleryImage->id }}]" id="caption" value="{{ $galleryImage->caption }}">
                                    </div>
                                @empty
                                    <div class=" col-md-12 text-center">No images to list.</div>
                                @endforelse
                            </ul>
                    </div>
                </div>
                @if($galleryImages->count() > 0)
                    <div class="col-xs-12 col-sm-12 cl-md-12 mb-5 text-center">
                        <input type="hidden" name="deleted_images" id="deleted-images">
                        <button type="submit" id="upload-file-and-submit" class="btn btn-primary">Update Images</button>
                    </div>
                @endif
            </form>
        </div>
    </div>

@endsection

@section('footer-scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
@endsection

@section('extra-footer-scripts')
    <script>
        var imageId = new Array();
        if(jQuery('.close-image').length >= 1) {
            jQuery('.close-image').on('click', function() {	
                var imageClass = jQuery(this).data('image-id');	
                imageId.push(jQuery(this).data('image-id'));
                jQuery('.image-'+imageClass).fadeOut(1000, function() { jQuery(this).remove(); });
                jQuery('#deleted-images').val(imageId);
            })
        }

        (function($) {
            $('#sortable').sortable({
                items: '.gallery-images'
            }).disableSelection();    
            $( "#sortable" ).on( "sortupdate", function( event, ui ) {
                var sortedData = $( "#sortable" ).sortable( "toArray");
                console.log(sortedData);
                $.ajax({
                    type:'PUT',
                    url:'{{ route('galleryimages.updatesortorder', $gallery) }}',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {'sortedData' : sortedData },
                    success:function(data){
                    }
                });
            } );
        })(jQuery);        

    </script>
@endsection