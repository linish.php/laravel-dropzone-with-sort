@extends('layouts.app')

@section('header-css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/min/dropzone.min.css" integrity="sha512-bbUR1MeyQAnEuvdmss7V2LclMzO+R9BzRntEE57WIKInFVQjvX7l7QZSxjNDt8bg41Ww05oHSh0ycKFijqD7dA==" crossorigin="anonymous" />
    <style>
        .dropzone .dz-preview .dz-image {
            width: 184px;
        }

        .dropzone .dz-preview .dz-remove {
            color: red;
        }

        .dz-image img {
            width: 100%;
            height: 100%;
        }
        .dropzone.dz-started .dz-message {
            display: block !important;
        }
        .dropzone {
            border: 2px dashed #028AF4 !important;;
        }
        .dropzone .dz-preview.dz-complete .dz-success-mark {
            opacity: 1;
        }
        .dropzone .dz-preview.dz-error .dz-success-mark {
            opacity: 0;
        }
        .dropzone .dz-preview .dz-error-message{
            top: 144px;
        }       

        .sortable {
            padding: 0;
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        .sortable li {
            display: inline-block;
            text-align: center;
            margin:5px;
        }
        li.sortable-placeholder {
            border: 1px dashed #CCC;
            background: none;
        }         
    </style>
@endsection

@section('content')

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-left">
                <h2>Add Gallery Images</h2>
            </div>
            <div class="float-right mb-4">
                <a class="btn btn-primary" href="{{ route('galleryimages.index', $gallery) }}">Back</a>
            </div>
        </div>
    </div>

    @if(count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="row">
        <div class="col-xs-12 col-sm-12 cl-md-12">
            <div class="card mb-4">
                <div class="card-header">Select Images</div>
                <div class="card-body">
                    <form id="gallery-dropzone" class="dropzone" action="{{ route('galleryimages.store', $gallery) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="text-center alert alert-info">Maximum 10 files at a time</div>                    
                    </form>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 cl-md-12 text-center">
            <button type="button" id="upload-file-and-submit" class="btn btn-primary">Upload Images</button>
        </div>

    </div>

@endsection

@section('footer-scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/min/dropzone.min.js" integrity="sha512-9WciDs0XP20sojTJ9E7mChDXy6pcO0qHpwbEJID1YVavz2H6QBz5eLoDD8lseZOb2yGT8xDNIV7HIe1ZbuiDWg==" crossorigin="anonymous"></script>
@endsection

@section('extra-footer-scripts')
    <script>
        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone("#gallery-dropzone", { 
            url: '{{ route('galleryimages.store', $gallery) }}',
            acceptedFiles: ".jpeg,.jpg,.png",
            addRemoveLinks: true,
            parallelUploads: 20,
            autoProcessQueue: false,
            uploadMultiple: true,
            maxFiles: 10,
            maxFilesize:5, //MB
            success: function (file, response) {
                window.location.href = "{{ route('galleryimages.index', $gallery) }}"
            },
            error: function (file, response) {
                return false;
            }
        });     

        jQuery('#upload-file-and-submit').click(function() {
            myDropzone.processQueue();
        });
    </script>
@endsection